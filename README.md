# ForgeRock DevOps Cloud Challenge

## Helm chart

This helm chart will deploy the challenge to a kubernetes cluster.  As part of
the deployment, it will create an `Ingress` for accessing the service.  The
query code is volume mounted into the container run by the `Deployment` pods.

The configuration and code can be found in `values.yaml`.  After deployment,
the `ConfigMap` will contain the configuration.  The API key can be found in
the `Secret`.

## Deployment

### Helm helper script

The `helm-deploy` helper script can be used to configure and deploy the
challenge to a kubernetes cluster.  Here is the usage:

```bash
$ ./helm-deploy --help

Usage:
  ./helm-deploy --apikey <apikey> [options]

Required flag:
  --apikey <apikey>  API key to use with the stock query service

Optional flags:
  --symbol <symbol>  Stock symbol to query (Default: MSFT)
  --ndays <ndays>    Number of days of stock history to query (Default: 7)
  --host <host>      Ingress host name to use (Default: challenge.localdomain)

Example:
  ./helm-deploy --apikey C227WD9W3LUVKVV9
```

Values specified with the deployment script will override the default values
found in `values.yaml`.

### Manual helm deployment

The `values.yaml` file may also be modified to update the challenge
configuration before running `helm` to deploy.

## Query challenge service

A simple curl command can be used to query the service.  Example:

```bash
$ curl http://challenge.localdomain
```

## Environment

The application environment can be viewed by visiting the `info.php` page.
e.g. http://challenge.localdomain/info.php

